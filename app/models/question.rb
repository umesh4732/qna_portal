class Question < ActiveRecord::Base
  belongs_to :user
  has_many :answers
  acts_as_taggable
  validates :body, presence: true
  scope :order_by_created_at, -> { order(created_at: :desc) }
end
