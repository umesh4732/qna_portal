class Answer < ActiveRecord::Base
  belongs_to :question
  belongs_to :user
  scope :order_by_created_at, -> { order(created_at: :desc) }
  scope :order_by_created_at_asc, -> { order(created_at: :asc) }
end
