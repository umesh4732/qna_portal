class AnswersController < ApplicationController
  before_action :authenticate_user!
  before_action :find_question, only: [:create, :marked_useful]
  before_action :find_answer, only: [:marked_useful]
  layout "admin"

  def index
    @answers = current_user.answers.order_by_created_at
  end

  def create
    answer = @question.answers.build(answer_param)
    if answer.save
      flash[:notice] = "Your answer posted successfully"
    else
      flash[:alert] = answer.errors.full_messages.to_sentence
    end
    redirect_to question_path(@question.id)
  end

  def marked_useful
    find_answered_user
    @answer.marked_useful? ? (@user.points = @user.points - 1) : (@user.points = @user.points + 1) 
    @answer.toggle(:marked_useful)
    if @user.save && @answer.save 
      flash[:notice] = "Point added successfully"
    else
      flash[:alert] = @user.errors.full_messages.to_sentence
    end
    redirect_to question_path(id: @question.id)
  end

  private

  def answer_param
    params.require(:answer).permit(:body).merge(user_id: current_user.id)
  end

  def find_question
    @question = Question.where(id: params[:question_id]).first
    unless @question.present?
      flash[:alert] = "Question not found"
      redirect_to questions_path
    end
  end

  def find_answer
    @answer = Answer.where(id: params[:id]).first
    unless @answer.present?
      flash[:alert] = "Answer not found"
      redirect_to questions_path
    end
  end

  def find_answered_user
    @user = @answer.user
    unless @user.present?
      flash[:alert] = "User not found"
      redirect_to questions_path
    end
  end

end
