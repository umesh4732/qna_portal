class QuestionsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_question, only: [:show]
  layout "admin"

  def index
    @questions = current_user.questions.order_by_created_at
  end

  def show
    @answer = current_user.answers.build
  end

  def new
    @question = current_user.questions.build
  end

  def create
    @question = current_user.questions.build(question_param)
    if @question.save
      flash[:notice] = "Question submitted successfully"
      redirect_to questions_path
    else
      flash[:error] = @question.errors.full_messages
      render "new"
    end
  end

  private

  def find_question
    @question = Question.where(id: params[:id]).first
    unless @question.present?
      flash[:alert] = "Question not found"
      redirect_to questions_path
    end
  end

  def question_param
    params.require(:question).permit(:body, :tag_list, :tag_list => [])
  end

end
