class DashboardController < ApplicationController
  before_action :authenticate_user!
  layout "admin"
  
  def index
    @questions = Question.order_by_created_at
  end
end
