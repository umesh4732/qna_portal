module QuestionsHelper
  def get_tags(question)
    raw question.tag_list.map { |t| link_to t }.join(', ')
  end
end
