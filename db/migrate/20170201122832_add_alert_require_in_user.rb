class AddAlertRequireInUser < ActiveRecord::Migration
  def change
    add_column :users, :alert_require, :boolean, :default => false
  end
end
