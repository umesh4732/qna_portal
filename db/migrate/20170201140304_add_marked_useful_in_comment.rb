class AddMarkedUsefulInComment < ActiveRecord::Migration
  def change
    add_column :answers, :marked_useful, :boolean, :default => false
    add_column :users, :points, :integer, :default => 0
  end
end
